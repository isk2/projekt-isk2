﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Logowanie.aspx.cs" Inherits="Logowanie" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <title>Zaloguj sie</title>
    <link rel='stylesheet' href='http://codepen.io/assets/libs/fullpage/jquery-ui.css'>
    <link rel="stylesheet" href="style.css" media="screen" type="text/css" />
</head>
<body>
    <div class="login-card">
            <h1>Zaloguj sie</h1><br>
            <form id="form1" runat="server">  
        <div>
            <span id="span">
                <asp:Label ID="Label1" runat="server" Text="Numer NIU"></asp:Label>
            </span>
            <asp:TextBox ID="TextBox1" runat="server" Text="" CssClass="text1"></asp:TextBox>
        </div>
        <div>
            <span id="span1">
                <asp:Label ID="Label2" runat="server" Text="Hasło "></asp:Label>
            </span>
            <asp:TextBox ID="haslo" runat="server" Text="" type="password" CssClass="text2"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="Zaloguj" class="login login-submit" runat="server" Text="Zaloguj" OnClick="Button1_Click" />
        </div>
        <asp:Label ID="Label3" runat="server" Font-Size="Small" Text="Nie udało ci się zalogować, spróbuj jeszcze raz." Visible="false"></asp:Label>
    </form>
    <script src='http://codepen.io/assets/libs/fullpage/jquery_and_jqueryui.js'></script>
</body>
</html>