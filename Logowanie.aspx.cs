﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logowanie : System.Web.UI.Page
{
    //dzięki temu connectionStringa nie trzeba zmieniać
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Logiczna.Administrator = false;
        Logiczna.Student = false;
        Logiczna.Wykladowca = false;
        Label3.Visible = false;
        haslo.TextMode = TextBoxMode.Password;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Sess
        string login = TextBox1.Text;
        string haslo = this.haslo.Text;
        if (login != "" && haslo != "")
        {
            if (SprawdzAdministratora(login, haslo))
            {
                Session["administratorLogin"] = login;
                Session["administratorHaslo"] = haslo;
                Logiczna.Administrator = true;
                Response.Redirect("PROTECTED/AdminLayer/StronaAdmin.aspx");
            }
            else if (SprawdzStudenta(login, haslo))
            {
                Session["studentLogin"] = login;
                Session["studentHaslo"] = haslo;
                Logiczna.Student = true;
                Response.Redirect("PROTECTED/StudentLayer/StronaStudent.aspx");

            }
            else if (SprawdzWykladowce(login, haslo))
            {
                Session["wykladowcaLogin"] = login;
                Session["wykladowcaHaslo"] = haslo;
                Logiczna.Wykladowca = true;
                Response.Redirect("PROTECTED/WykladowcaLayer/StronaWykladowca.aspx");
            }
            else
            {
                Logiczna.Administrator = false;
                Logiczna.Student = false;
                Logiczna.Wykladowca = false;
                Label3.Visible = true;
            }

        }
    }
    private bool SprawdzAdministratora(string login, string haslo)
    {
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        String query = "select imie from Administratorzy where Administratorzy.login = @Login and Administratorzy.haslo = @Pass";
        SqlCommand command = new SqlCommand(query,connection);
        command.Parameters.AddWithValue("@Login", login);
        command.Parameters.AddWithValue("@Pass", haslo);
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        adapter.SelectCommand = command;
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        connection.Close();
        if (dt.Rows.Count != 0)
        {
            return true;
        }
        return false;
    }
    private bool SprawdzStudenta(string login, string haslo)
    {
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        String query = "select imie from Studenci where Studenci.nr_indeksu = @Login and Studenci.haslo = @Pass";
        SqlCommand command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@Login", login);
        command.Parameters.AddWithValue("@Pass", haslo);
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        connection.Close();
        if (dt.Rows.Count != 0)
        {
            return true;
        }
        return false;
    }
    private bool SprawdzWykladowce(string login, string haslo)
    {
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        String query = "select imie from Wykladowcy where Wykladowcy.login = @Login and Wykladowcy.haslo = @Pass";
        SqlCommand command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@Login", login);
        command.Parameters.AddWithValue("@Pass", haslo);
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        connection.Close();
        if (dt.Rows.Count != 0)
        {
            return true;
        }
        return false;
    }
}