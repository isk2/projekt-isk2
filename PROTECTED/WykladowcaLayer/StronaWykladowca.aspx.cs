﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

public partial class PROTECTED_WykladowcaLayer_StronaWykladowca : System.Web.UI.Page
{

    String opcja;
    String opcja2;
    //        String connectionString = 
    //@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";

    String connection = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
    SqlConnection objConn;

    protected void Page_Load(object sender, EventArgs e)
    {
        //DropDownList2.Enabled = false;

        objConn = new SqlConnection(connection);
        objConn.Open();

        if (!Page.IsPostBack)
        {
            //daneStudenta();
            //daneStudentPrzedmiot();
            DropDownList1_Fill();
        }
    }

    protected void DropDownList1_Fill()
    {
        string login = Session["wykladowcaLogin"].ToString();
        using(DziekanatDBContextDataContext db = new DziekanatDBContextDataContext())
        {
            Wykladowcy wykładowcainf = (from wyk in db.Wykladowcies where wyk.login == login select wyk).FirstOrDefault();        
            var przedwyk = from wykprzed in db.PrzedmiotWykladowcas where wykładowcainf.Id == wykprzed.idwykladowcy select wykprzed;
            var rokakademicki = from rok in przedwyk select rok.rokakademicki;
            var sem = from semestr in przedwyk select semestr.semestr;
            var kierunki = from kierunek in przedwyk select kierunek.Kierunki.nazwa;

            //dodane
            var przedmioty = from przedmiot in przedwyk select przedmiot.Przedmioty.nazwa;
            //

            DropDownList1.DataSource = kierunki;
            DropDownList1.DataBind();
            DropDownList3.DataSource = rokakademicki;
            DropDownList3.DataBind();
            DropDownList4.DataSource = sem;
            DropDownList4.DataBind();
            //dodane
            DropDownList7.DataSource = przedmioty;
            DropDownList7.DataBind();
            //
        }
        //String stringSQL = "select idkierunku from Studenci group by idkierunku";
        //SqlCommand objCmd = new SqlCommand(stringSQL, objConn);

        ////Druga możliwość komendy
        ////SqlCommand objCmd = new SqlCommand();
        ////objCmd.Connection = objConn;
        ////objCmd.CommandText = "select kierunek from Studenci group by kierunek";
        ////objCmd.CommandType = CommandType.Text;

        //SqlDataReader dt = objCmd.ExecuteReader();

        ////Druga możliwość wykonania
        ////SqlDataAdapter adapter = new SqlDataAdapter(objCmd);
        ////DataTable dt = new DataTable();
        ////adapter.Fill(dt);

        ////Trzecia możliwość wykonania
        ////DataTable dt  = new DataTable();
        ////dt.Load(objCmd.ExecuteReader());

        //DropDownList1.DataSource = dt;
        //DropDownList1.DataTextField = "idkierunku";
        //DropDownList1.DataValueField = "idkierunku";
        //DropDownList1.DataBind();
    }

    //protected void daneStudenta()
    //{
    //    String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
    //    SqlConnection connection = new SqlConnection(connectionString);
    //    SqlCommand command = new SqlCommand();
    //    command.Connection = connection;
    //    command.CommandText = String.Format("select id, nr_indeksu, imie, nazwisko, idkierunku, aktualny_sem from Studenci"); //aktualny_sem jako ocena
    //    //musi byc Id w query i jako predefiniowana kolumna poniewaz zeby edytowac potrzebne jest DataKeyNames z ustawionym kluczem na Id ale zeby nie bylo wyswietlane trzeba dac dla tej kolumny Visible=false
    //    command.CommandType = CommandType.Text;
    //    SqlDataAdapter adapter = new SqlDataAdapter(command);
    //    DataTable dt = new DataTable();
    //    adapter.Fill(dt);
    //    GridView1.DataSource = dt;
    //    GridView1.DataBind();
    //    GridView1.Visible = true;
    //    connection.Close();

    //}
    //protected void daneStudentPrzedmiot()
    //{
    //    String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
    //    SqlConnection connection = new SqlConnection(connectionString);
    //    SqlCommand command = new SqlCommand();
    //    command.Connection = connection;

    //    //command.CommandText = String.Format("select id, nr_indeksu, imie, nazwisko, idkierunku, aktualny_sem from Studenci"); //aktualny_sem jako ocena
    //    command.CommandText = String.Format("select id from Studenci");
    //    //command.CommandText = String.Format("select id, idstudenta, idprzedmiotwykladowca, ocena from StudentPrzedmiot"); //aktualny_sem jako ocena
    //    //musi byc Id w query i jako predefiniowana kolumna poniewaz zeby edytowac potrzebne jest DataKeyNames z ustawionym kluczem na Id ale zeby nie bylo wyswietlane trzeba dac dla tej kolumny Visible=false
    //    command.CommandType = CommandType.Text;
    //    SqlDataAdapter adapter = new SqlDataAdapter(command);
    //    DataTable dt = new DataTable();
    //    adapter.Fill(dt);
    //    GridView2.DataSource = dt;
    //    GridView2.DataBind();
    //    GridView2.Visible = true;
    //    connection.Close();
    //}
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        string login = Session["wykladowcaLogin"].ToString();
        using (DziekanatDBContextDataContext db = new DziekanatDBContextDataContext())
        {
            Wykladowcy wykładowcainf = (from wyk in db.Wykladowcies where wyk.login == login select wyk).FirstOrDefault();
            string skierunek = DropDownList1.SelectedValue;
            var kierunek = (from k in db.Kierunkis where k.nazwa == skierunek select k).FirstOrDefault();
            var przedwyk = from wykprzed in db.PrzedmiotWykladowcas where wykładowcainf.Id == wykprzed.idwykladowcy && wykprzed.idkierunku == kierunek.Id select wykprzed;
            var rokakademicki = from rok in przedwyk select rok.rokakademicki;
            var sem = from semestr in przedwyk select semestr.semestr;

            //dodane
            var przedmioty = from przedmiot in przedwyk select przedmiot.Przedmioty.nazwa;
            //


            DropDownList3.DataSource = rokakademicki;
            DropDownList3.DataBind();
            DropDownList4.DataSource = sem;
            DropDownList4.DataBind();
            //dodane
            DropDownList7.DataSource = przedmioty;
            DropDownList7.DataBind();
            //
        }
    }

    protected void DropDownList1_Method()
    {

        //Label1.Text = "";
        //DropDownList2.Enabled = true;
        //opcja = DropDownList1.SelectedValue;

        //String stringSQL = "select nr_indeksu from Studenci where idkierunku='" + opcja + "'";
        //SqlCommand objCmd = new SqlCommand(stringSQL, objConn);

        //SqlDataReader dt = objCmd.ExecuteReader();

        //DropDownList2.DataSource = dt;
        //DropDownList2.DataTextField = "nr_indeksu";
        //DropDownList2.DataValueField = "nr_indeksu";
        //DropDownList2.DataBind();

        //dt.Close();
        //dt = null;

        //DropDownList1_Selection();
        //wyswietlenie na gridview studentow z okreslonego kierunku
    }

    protected void DropDownList1_Selection()
    {
       // DropDownList2.Enabled = true;
        opcja = DropDownList1.SelectedValue;

        String stringSQL = "select id, nr_indeksu, imie, nazwisko, idkierunku, aktualny_sem from Studenci where idkierunku='" + opcja + "'";
        SqlCommand objCmd = new SqlCommand(stringSQL, objConn);

        SqlDataReader dt = objCmd.ExecuteReader();

        //GridView1.DataSource = dt;
        //GridView1.DataBind();

        dt.Close();
        dt = null;
    }

    //protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    DropDownList2_Method();
    //}

    //protected void DropDownList2_Method()
    //{
    //    Label1.Text = "";
    //    DropDownList2.Enabled = true;
    //    opcja2 = DropDownList2.SelectedValue;

    //    String strSQL = "select id, nr_indeksu, imie, nazwisko, idkierunku, aktualny_sem from Studenci where nr_indeksu='" + opcja2 + "'";//aktualny_sem jako ocena
    //    //jest potrzebne jako źródło do predefiniowanych kolumn w widoku gridview
    //    //żeby nie dublowało kolumn (predefiniowane i te stad z kodu) trzeba zaznaczyc przy wlasciwosciach gridview AutoGenerateColumns na false

    //    SqlDataReader dtReader;
    //    SqlCommand objCmd = new SqlCommand(strSQL, objConn);
    //    dtReader = objCmd.ExecuteReader();

    //    GridView1.DataSource = dtReader;
    //    GridView1.DataBind();

    //    dtReader.Close();
    //    dtReader = null;
    //}

    protected void Page_UnLoad()
    {
        objConn.Close();
        objConn = null;
    }

    ////Usuwanie rekordow - Nieaktywne: ta metoda, w gridview powiazanie zdarzenie z ta metoda, predefiniowana kolumna delete
    //protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    DropDownList2.Enabled = true;

    //    //kasowanie po id, trzeba dodać do Gridview DataKeyNames="Id"
    //    String id = GridView1.DataKeys[e.RowIndex]["Id"].ToString();
    //    String connectionString = @"Data Source=KAYAK-KOMPUTER;Initial Catalog=Dziekanat;Integrated Security=True";
    //    SqlConnection connection = new SqlConnection(connectionString);
    //    SqlCommand command = new SqlCommand();
    //    command.Connection = connection;
    //    command.CommandText = String.Format("delete from Studenci where id ={0}", id);
    //    command.CommandType = CommandType.Text;
    //    connection.Open();
    //    command.ExecuteNonQuery();
    //    connection.Close();
    //    daneStudenta();
    //}

    //protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    //{
    //    DropDownList2.Enabled = true;

    //    //edycja-update po id, trzeba dodać do Gridview DataKeyNames="Id"
    //    String id = GridView1.DataKeys[e.RowIndex]["Id"].ToString();
    //    String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
    //    SqlConnection connection = new SqlConnection(connectionString);
    //    SqlCommand command = new SqlCommand();
    //    command.Connection = connection;
    //    for (int i = 0; i < GridView1.Columns.Count; i++)
    //    {
    //        DataControlFieldCell objCell = (DataControlFieldCell)GridView1.Rows[e.RowIndex].Cells[i];
    //        GridView1.Columns[i].ExtractValuesFromCell(e.NewValues, objCell, DataControlRowState.Edit, false);
    //    }
    //    command.CommandText = string.Format("update Studenci set aktualny_sem='{0}' where id='{1}'", e.NewValues["aktualny_sem"], id);
    //    //zmiana jedynie pola "aktualny_sem" dla danego studenta (po id)
    //    connection.Open();
    //    command.ExecuteNonQuery();
    //    connection.Close();
    //    GridView1.EditIndex = -1;

    //    DropDownList1_Selection();
    //    //wyswietlenie na gridview studentow z okreslonego kierunku
    //    Label1.Text = "Ocena została zmieniona";
    //}

    //protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    //{
    //    DropDownList2.Enabled = true;

    //    GridView1.EditIndex = -1;

    //    DropDownList1_Selection();
    //    //wyswietlenie na gridview studentow z okreslonego kierunku
    //}

    //protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    //{
    //    Label1.Text = "";
    //    //zeby mozna bylo edytowac tylko jedna kolumne (tutaj oceny) trzeba reszcie dac ReadOnly="true"
    //    GridView1.EditIndex = e.NewEditIndex;

    //    DropDownList2_Method();
    //    //wyswietlenie na gridview tylko wybranego studenta
    //}
    protected void Button1_Click(object sender, EventArgs e)
    {
        ShowStudentsBySubject();
    }

    private void ShowStudentsBySubject()
    {
        string login = Session["wykladowcaLogin"].ToString();
        //dodane
        string przedmiot = DropDownList7.SelectedValue;
        //
        string kierunek = DropDownList1.SelectedValue;
        string rokakademicki = DropDownList3.SelectedValue;
        int semestr = Int32.Parse(DropDownList4.SelectedValue);
        using (DziekanatDBContextDataContext db = new DziekanatDBContextDataContext())
        {
            var wykladowca = (from wyk in db.Wykladowcies where wyk.login == login select wyk).FirstOrDefault();
            var k = (from kierunekk in db.Kierunkis where kierunekk.nazwa == kierunek select kierunekk).FirstOrDefault();
            /*iterujemy po tablicy db.Kierunkis,
          *gdzie kolejny element tej tablicy jest dostępny za pomocą zmiennej kierunekk
          *Operatorem select wsadzamy coś do naszego wyniku czyli zmiennej k 
          */
            //kierunki (k) = z kierunekk w db.Kierunkis gdzie nazwakierunku jest pobierana z DropDownList1 bierz kierunekk


            //dodane
            var przedm = (from przedmm in db.Przedmioties where przedmm.nazwa == przedmiot select przedmm).FirstOrDefault();
            //


            var wykprzed = (from przedwyk in db.PrzedmiotWykladowcas
                            //iterujemy po tablicy db.PrzedmitWykladowcas,
                            // kolejny element tej tablicy jest dostępny za pomocą zmiennej przedwyk
                            where przedwyk.idkierunku == k.Id
                            //do elementu tablicy (idkierunku) przypisujemy pobrany z tabeli bazy db.Kierunkis kierunek (k)
                            && przedwyk.idwykladowcy == wykladowca.Id 
                            //do kolejnego elementu tablicy (idwykladowcy) przypisujemy zalogowanego wlasnie wykladowce
                            && przedwyk.rokakademicki == rokakademicki
                            //do do kolejnego elementu tablicy (rokakademicki) przypisujemy wartość pobraną z DropDownList1
                            && przedwyk.semestr == semestr
                            //do do kolejnego elementu tablicy (semstr) przypisujemy wartość pobraną z DropDownList4
                            && przedwyk.idprzedmiotu == przedm.Id
                            //dodane
                            select przedwyk).FirstOrDefault();
            /*iterujemy po tablicy db.PrzedmitWykladowcas,
           *gdzie kolejny element tej tablicy jest dostępny za pomocą zmiennej przedwyk
           *Operatorem select wsadzamy coś do naszego wyniku czyli zmiennej wykprzed 
           */

            // wykladowcaPrzedmiot = z przedwyk w db.PrzedmiotWykladowcas gdzie kierunek && wykladowca && rok akademicki && semestr BIERZ przedwyk
            var studenci = from studentprzed in db.StudentPrzedmiots
                           //iterujemy po tablicy db.StudentPrzedmiots,
                           //kolejny element tej tablicy jest dostępny za pomocą zmiennej studentprzed
                           where studentprzed.idprzedmiotwykladowca == wykprzed.Id
                           //do elementu tablicy (idprzedmiotwykladowca) przypisujemy |
                           //wszystkie dane zwrócone ze zmiennej wykprzed
                           select new { 
                               NrIndeksu = studentprzed.Studenci.Id,
                               Imie = studentprzed.Studenci.imie,
                               Nazwisko = studentprzed.Studenci.nazwisko,
                               Ocena = studentprzed.ocena,
                               IdPrzedmiotProwadzacy = wykprzed.Id 
                           };
            GridView3.DataSource = studenci;
            GridView3.DataBind();
        }
    }
    protected void GridView3_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView3.EditIndex = e.NewEditIndex;
        ShowStudentsBySubject();
    }
    protected void GridView3_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView3.EditIndex = -1;
        ShowStudentsBySubject();
    }
    protected void GridView3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = (GridViewRow)GridView3.Rows[e.RowIndex];
        int id = Int32.Parse(GridView3.DataKeys[e.RowIndex]["NrIndeksu"].ToString());
        string ocena = ((TextBox)row.Cells[4].Controls[0]).Text;
        int idprzedmiotwyk = Int32.Parse(GridView3.DataKeys[e.RowIndex]["IdPrzedmiotProwadzacy"].ToString());
        using(DziekanatDBContextDataContext db = new DziekanatDBContextDataContext())
        {
            var studentPrzedmiot = (from studentPrzed in db.StudentPrzedmiots
                                    where studentPrzed.idstudenta == id && studentPrzed.idprzedmiotwykladowca == idprzedmiotwyk
                                    select studentPrzed).FirstOrDefault();
            studentPrzedmiot.ocena = ocena;
            db.SubmitChanges();
        }
        GridView3.EditIndex = -1;
        ShowStudentsBySubject();
    }
    protected void DropDownList7_SelectedIndexChanged(object sender, EventArgs e)
    {
        string login = Session["wykladowcaLogin"].ToString();
        using (DziekanatDBContextDataContext db = new DziekanatDBContextDataContext())
        {
            Wykladowcy wykładowcainf = (from wyk in db.Wykladowcies where wyk.login == login select wyk).FirstOrDefault();
            string skierunek = DropDownList1.SelectedValue;
            var przedmiot = (from przed in db.Przedmioties where przed.nazwa == DropDownList7.SelectedValue select przed).FirstOrDefault();
            var kierunek = (from k in db.Kierunkis where k.nazwa == skierunek select k).FirstOrDefault();
            var przedwyk = from wykprzed in db.PrzedmiotWykladowcas where wykładowcainf.Id == wykprzed.idwykladowcy && wykprzed.idkierunku == kierunek.Id && wykprzed.idprzedmiotu == przedmiot.Id select wykprzed;
            var rokakademicki = from rok in przedwyk select rok.rokakademicki;
            var sem = from semestr in przedwyk select semestr.semestr;

            DropDownList3.DataSource = rokakademicki;
            DropDownList3.DataBind();
            DropDownList4.DataSource = sem;
            DropDownList4.DataBind();
        }
    }
}