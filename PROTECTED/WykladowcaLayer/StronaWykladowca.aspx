﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/WykladowcaLayer/MasterWykladowca.master" AutoEventWireup="true" CodeFile="StronaWykladowca.aspx.cs" Inherits="PROTECTED_WykladowcaLayer_StronaWykladowca" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h4>Wybierz kierunek, rok akademicki, semestr, przedmiot.<br />
    Po wybraniu w tabeli studentów można wystawić ocenę danemu studentowi.</h4>
    <div id="wykladowcawybor">
        <span id="pierwszy"><p>Kierunek:</p><asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" ></asp:DropDownList></span>
        <span><p>Rok akademicki:</p><asp:DropDownList ID="DropDownList3" runat="server" ></asp:DropDownList></span>
        <span><p>Semestr:</p><asp:DropDownList ID="DropDownList4" runat="server" ></asp:DropDownList></span>
        <span><p>Przedmiot:</p><asp:DropDownList ID="DropDownList7" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList7_SelectedIndexChanged" ></asp:DropDownList></span>
        <span id="przyciski"><asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Wyświetl studentów" /></span>
    </div>

    <br />
    <h4>Lista studentów</h4>
    <asp:GridView ID="GridView3" runat="server" ForeColor="White" HeaderStyle-BackColor="#060915" FooterStyle-BackColor="#060915" GridLines="Horizontal" RowStyle-Wrap="true" AutoGenerateColumns="False" DataKeyNames="NrIndeksu,IdPrzedmiotProwadzacy" EnableModelValidation="True" OnRowCancelingEdit="GridView3_RowCancelingEdit" OnRowEditing="GridView3_RowEditing" OnRowUpdating="GridView3_RowUpdating" >
        <AlternatingRowStyle BackColor="#1E456B" ForeColor="White" />
        <Columns>
            <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="http://www.cerchem.pl/test/zmienwykl.png" CancelImageUrl="http://www.cerchem.pl/test/anulujwykl.png" UpdateImageUrl="http://www.cerchem.pl/test/okwykl.png" ItemStyle-HorizontalAlign="Center"  />
            <asp:BoundField DataField="NrIndeksu" HeaderText="Nr Indeksu" ItemStyle-CssClass="jasnycenter" HeaderStyle-CssClass="jasnycenter" />
            <asp:BoundField DataField="Imie" HeaderText="Imię" />
            <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" ItemStyle-CssClass="jasnycenter" HeaderStyle-CssClass="jasnycenter" />
            <asp:BoundField DataField="Ocena" HeaderText="Ocena" />
            <asp:BoundField DataField="IdPrzedmiotProwadzacy" HeaderText="Id prowadzącego przedmiot" Visible="False" ItemStyle-CssClass="jasnycenter" HeaderStyle-CssClass="jasnycenter" />
        </Columns>
    </asp:GridView>

</asp:Content>

