﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Web.Security;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;


public partial class PROTECTED_StudentLayer_StronaStudent : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            PobierzSemestr();
        }
    }
    protected void dane_Click(object sender, EventArgs e)
    {

        Server.Transfer("DaneStudent.aspx");

    }
    protected void PobierzSemestr()
    {
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();

        string zmienna = (string)Session["studentLogin"];
        command.Connection = connection;
        command.CommandText = String.Format("select semestr, CAST(semestr as varchar(2)) +' / '+ rokakademicki as nazwa from PrzedmiotWykladowca JOIN StudentPrzedmiot ON PrzedmiotWykladowca.Id=StudentPrzedmiot.idprzedmiotwykladowca JOIN Studenci ON StudentPrzedmiot.idstudenta=Studenci.Id AND Studenci.nr_indeksu=@zmienna");
        command.Parameters.AddWithValue("@zmienna", zmienna);
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();

        adapter.Fill(dt);
        if (dt.Rows.Count == 0)
        {
            DropDownList1.DataSource = dt;
            DropDownList1.DataValueField = "semestr";
            DropDownList1.DataTextField = "nazwa";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, new ListItem("Brak rekordów", String.Empty));
        }
        else
        {
            DropDownList1.DataSource = dt;
            DropDownList1.DataValueField = "semestr";
            DropDownList1.DataTextField = "nazwa";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, new ListItem("Wybierz Semestr", String.Empty));
            
        }
        connection.Close();

       // command.Connection = connection;
       // 
       // command.CommandText = String.Format("select aktualny_sem from Studenci where Studenci.nr_indeksu = '{0}' ",zmienna);
       //// command.CommandText = String.Format("select semestr from PrzedmiotWykladowca where Studenci.nr_indeksu = '{0}' ", zmienna); 
       // command.CommandType = CommandType.Text;

       // SqlDataAdapter adapter = new SqlDataAdapter(command);
       // DataSet ds = new DataSet();
       // adapter.Fill(ds);

       // object value = ds.Tables[0].Rows[0][0];
       // value.ToString();
       // int c = Convert.ToInt32(value);

       // List<int> listaSemestrow = new List<int>();
       // do
       // {
       //     listaSemestrow.Add(c);
       //     c--;
       // } while (c != 0);

       // DropDownList1.DataSource = listaSemestrow;
       // DropDownList1.DataBind();
       // DropDownList1.Items.Insert(0, new ListItem("Wybierz Semestr", String.Empty));

       // connection.Close();
    }
    protected void oceny_Click(object sender, EventArgs e)
    {
        Session["wybranysemestr"] = DropDownList1.SelectedValue;
        Response.Redirect("OcenyStudenta.aspx");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }

    String opcja; 
    String connection = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;  
    SqlConnection objConn;
    String nr_indeksu;
    String imie;
    String nazwisko;
    String nadawca;


    protected void DropDownList1_Fill()
    {

        //Pierwsza możliwość komendy
        String stringSQL = "select email, imie, nazwisko from Wykladowcy";
        SqlCommand objCmd = new SqlCommand(stringSQL, objConn);

        //Druga możliwość komendy
        //SqlCommand objCmd = new SqlCommand();
        //objCmd.Connection = objConn;
        //objCmd.CommandText = "select nazwisko from Wykladowcy";
        //objCmd.CommandType = CommandType.Text;

        //Pierwsza możliwość wykonania
        SqlDataReader dt = objCmd.ExecuteReader();

        //Druga możliwość wykonania
        //SqlDataAdapter adapter = new SqlDataAdapter(objCmd);
        //DataTable dt = new DataTable();
        //adapter.Fill(dt);

        //Trzecia możliwość wykonania
        //DataTable dt  = new DataTable();
        //dt.Load(objCmd.ExecuteReader());

        //Pierwsza możliwość wypełnienia listy z jedną wartością
        //DropDownList1.DataSource = dt;
        //DropDownList1.DataTextField = "nazwisko";
        //DropDownList1.DataValueField = "email";
        //DropDownList1.DataBind();

        //Druga możliwość wypełnienia listy z połączoną wartością
        while (dt.Read())
        {
            string wartosc = dt.GetSqlString(0).Value;
            string tekst = dt.GetSqlString(1).Value + " " + dt.GetSqlString(2).Value;

            DropDownList1.Items.Add(new ListItem(tekst, wartosc));
        }
    }

    protected void DropDownList1_SelectedIndexChanged1(object sender, EventArgs e)
    {
        opcja = DropDownList1.SelectedValue;
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        //Sprawdzanie kto jest zalogowany
        if (Session["studentLogin"] != null)
        {
            String login = Session["studentLogin"].ToString();
            String checkSQL = "select nr_indeksu, imie, nazwisko from Studenci where nr_indeksu=@login" ;
            SqlCommand checkCmd = new SqlCommand(checkSQL, objConn);
            checkCmd.Parameters.AddWithValue("@login", login);
            SqlDataReader data = checkCmd.ExecuteReader();
            while (data.Read())
            {
                nr_indeksu = (string)data["nr_indeksu"];
                imie = (string)data["imie"];
                nazwisko = (string)data["nazwisko"];

                //Druga możliwość
                //nr_indeksu = data[0].ToString();
                //imie = data[1].ToString();
                //nazwisko = data[2].ToString();
            }
            data.Close();
            nadawca = nr_indeksu + " " + imie + " " + nazwisko;
        }

        String odbiorca = opcja;
        MailMessage message = new MailMessage();
        message.From = new MailAddress("sggwdziekanat@gmail.com");
        message.To.Add(new MailAddress(odbiorca));
        message.Subject = "Wiadomość od studenta: " + nadawca;
        message.Body = TextBox1.Text;

        SmtpClient smtp = new SmtpClient("smtp.gmail.com");
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = new NetworkCredential("sggwdziekanat@gmail.com", "haslodziekanat");
        smtp.EnableSsl = true;
        smtp.Port = 587;

        smtp.Send(message);

        Response.Redirect("Message.aspx");
    }
}