﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class PROTECTED_StudentLayer_DaneStudent : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DaneStudent();
        }

    }
    protected void DaneStudent()
    {
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText =
        String.Format("select Id, nr_indeksu, imie, nazwisko, adres, data_urodzenia, pesel, data_rozpoczecia, aktualny_sem, tryb_studiow, Idkierunku, mail, haslo from Studenci where Studenci.nr_indeksu = @nr_indeksu and Studenci.haslo = @haslo");
        command.Parameters.AddWithValue("@nr_indeksu",(string)Session["studentLogin"]);
        command.Parameters.AddWithValue("@haslo",(string)Session["studentHaslo"]);
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        GridView_DaneStudent.DataSource = dt;
        GridView_DaneStudent.DataBind();
        GridView_DaneStudent.Visible = true;
        connection.Close();
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        String id = GridView_DaneStudent.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        for (int i = 0; i < GridView_DaneStudent.Columns.Count; i++)
        {
            DataControlFieldCell objCell = (DataControlFieldCell)GridView_DaneStudent.Rows[e.RowIndex].Cells[i];
            GridView_DaneStudent.Columns[i].ExtractValuesFromCell(e.NewValues, objCell, DataControlRowState.Edit, false);
        }
        try
        {
            if (String.IsNullOrEmpty(e.NewValues["imie"].ToString())||e.NewValues["nazwisko"].ToString().Length==0|| e.NewValues["adres"].ToString().Length == 0 || e.NewValues["data_urodzenia"].ToString().Length == 0 || e.NewValues["haslo"].ToString().Length == 0)
            {
                
                throw new Exception();

            }
            else
            {
                command.CommandType = CommandType.Text;
                command.CommandText = "update Studenci set imie=@imie, nazwisko=@nazwisko, adres=@adres, data_urodzenia=@data_urodzenia, haslo=@haslo where Id=@Id";
                command.Parameters.AddWithValue("@id",id);
                command.Parameters.AddWithValue("@imie",e.NewValues["imie"]);
                command.Parameters.AddWithValue("@nazwisko",e.NewValues["nazwisko"]);
                command.Parameters.AddWithValue("@adres",e.NewValues["adres"]);
                command.Parameters.AddWithValue("@data_urodzenia",e.NewValues["data_urodzenia"]);
                command.Parameters.AddWithValue("@haslo", e.NewValues["haslo"]);
                Session["studentHaslo"] = e.NewValues["haslo"];
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                GridView_DaneStudent.EditIndex = -1;
                DaneStudent();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + "Pomyślnie zaktualizowano" + "');", true);
            }
            
        }
        catch (Exception ex)
        {

            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + "Wystąpił błąd" + "');", true);

        }
        
        
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView_DaneStudent.EditIndex = e.NewEditIndex;
        DaneStudent();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView_DaneStudent.EditIndex = -1;
        DaneStudent();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_DaneStudent.PageIndex = e.NewPageIndex;
        DaneStudent();
    }
    protected void Powrot_Click(object sender, EventArgs e)
    {
        Server.Transfer("StronaStudent.aspx");
    }
}