﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/StudentLayer/MasterStudent.master" AutoEventWireup="true" CodeFile="StronaStudent.aspx.cs" Inherits="PROTECTED_StudentLayer_StronaStudent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="szukanko">
        <span>Semestr: <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
        <asp:Button ID="oceny" runat="server" Text="Wyświetl oceny" OnClick="oceny_Click" />
        <asp:Button ID="dane" runat="server" Text="Wyświetl dane" OnClick="dane_Click" />
        </span>
    </div>

        <div>
            <asp:GridView ID="GridView1" runat="server" Visible="False" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" EnableModelValidation="True">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:BoundField DataField="nr_indeksu" HeaderText="Numer indeksu" />
                    <asp:BoundField DataField="imie" HeaderText="Imię" />
                    <asp:BoundField DataField="nazwisko" HeaderText="Nazwisko" />
                    <asp:BoundField DataField="adres" HeaderText="Adres" />
                    <asp:BoundField DataField="data_urodzenia" HeaderText="Data urodzenia" />
                    <asp:BoundField DataField="pesel" HeaderText="Pesel" />
                    <asp:BoundField DataField="data_rozpoczecia" HeaderText="Data rozpoczęcia studiów" />
                    <asp:BoundField DataField="aktualny_sem" HeaderText="Aktualny semestr" />
                    <asp:BoundField DataField="tryb_studiow" HeaderText="Tryb studiów" />
                    <asp:BoundField DataField="idkierunku" HeaderText="Kierunek" />
                    <asp:BoundField DataField="mail" HeaderText="Adres e-mail" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>

    <div id="cen"><h3>Wiadomość do wykładowcy</h3></div>
    <div id="wyslijwiadomosc">
    <span id="pierwszy"><p>Wybierz wykładowcę:</p><asp:DropDownList ID="DropDownList2" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged1"></asp:DropDownList></span>
    
    <span><p>Wpisz treść wiadomości:</p></span>
    <span><asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine"></asp:TextBox></span>
    <span id="przyciski"><asp:Button ID="Button1" runat="server" Text="Wyślij" OnClick="Button1_Click1" /></span>
    </div>
        </div>
</asp:Content>

