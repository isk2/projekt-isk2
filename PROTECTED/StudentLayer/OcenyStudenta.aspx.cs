﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class PROTECTED_StudentLayer_OcenyStudenta : System.Web.UI.Page
{
    private int wybranysemestr;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!Page.IsPostBack)
        {
            try
            {
                wybranysemestr = Convert.ToInt32((string)(Session["wybranysemestr"]));
                PobierzOceny();
            }
            catch(Exception ex)
            {};
        }
    }
    void PobierzOceny()
    {
  
        string nrIndeksu = (string)Session["studentLogin"];
        using (DziekanatDBContextDataContext db = new DziekanatDBContextDataContext())
        {
            var st = (from student in db.Studencis where student.nr_indeksu == nrIndeksu select student).FirstOrDefault();
            var stprzed = st.StudentPrzedmiots;
            var przedst = from asd in stprzed where asd.PrzedmiotWykladowca.semestr==wybranysemestr select new { asd.PrzedmiotWykladowca.Przedmioty.nazwa, asd.PrzedmiotWykladowca.Wykladowcy.imie, asd.PrzedmiotWykladowca.Wykladowcy.nazwisko, asd.ocena };
            GridView1.DataSource = przedst;
          
            GridView1.DataBind();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Server.Transfer("StronaStudent.aspx");
    }
}