﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PROTECTED_StudentLayer_MasterStudent : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string menuName = "";
        string menuContent = "";
        if (!Logiczna.Student)
        {
            Response.Redirect("~/Logowanie.aspx");
        }
        if (Logiczna.Student == true)
        {
            menuName = "Student";
            menuContent = "<li class=\"\">" +
                             "<a href=\"../StudentLayer/StronaStudent.aspx\"><span>Strona Studenta</span></a>" +
                          "</li>";
        }
        string content = "<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">{0}<span class=\"caret\"></span></a><ul class=\"dropdown-menu\">{1}</ul>";

        menu_li.InnerHtml = string.Format(content, menuName, menuContent);
    }
    protected void Wyloguj_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Session.Abandon();
        Session.RemoveAll();
        Response.Redirect("~/Logowanie.aspx");
    }
}
