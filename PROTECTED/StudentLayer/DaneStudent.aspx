﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/StudentLayer/MasterStudent.master" AutoEventWireup="true" CodeFile="DaneStudent.aspx.cs" Inherits="PROTECTED_StudentLayer_DaneStudent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <asp:GridView ID="GridView_DaneStudent" runat="server" Visible="False" ForeColor="white" HeaderStyle-BackColor="#180202" FooterStyle-BackColor="#180202" GridLines="Horizontal" RowStyle-Wrap="true" AutoGenerateColumns="False" EnableModelValidation="True" DataKeyNames="Id" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">
             <Columns>
                    <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="http://www.cerchem.pl/test/zmienstud.png" CancelImageUrl="http://www.cerchem.pl/test/anulujstud.png" UpdateImageUrl="http://www.cerchem.pl/test/okstud.png" ItemStyle-HorizontalAlign="Center"  />
                    <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
                    <asp:BoundField DataField="nr_indeksu" HeaderText="Nr indeksu" ReadOnly="True" ItemStyle-CssClass="jasnycenter" HeaderStyle-CssClass="jasnycenter" />
                    <asp:BoundField DataField="imie" HeaderText="Imię" />
                    <asp:BoundField DataField="nazwisko" HeaderText="Nazwisko" ItemStyle-CssClass="jasnycenter" HeaderStyle-CssClass="jasnycenter" />
                    <asp:BoundField DataField="idkierunku" HeaderText="Kierunek" ReadOnly="True" />
                    <asp:BoundField DataField="aktualny_sem" HeaderText="Semestr" ReadOnly="True" ItemStyle-CssClass="jasnycenter" HeaderStyle-CssClass="jasnycenter" />
                    <asp:BoundField DataField="tryb_studiow" HeaderText="Tryb" ReadOnly="True" />
                    <asp:BoundField DataField="data_rozpoczecia" HeaderText="Rozpoczęcie" ReadOnly="True" DataFormatString="{0:d}" ItemStyle-CssClass="jasnycenter" HeaderStyle-CssClass="jasnycenter" />
                    <asp:BoundField DataField="mail" HeaderText="E-mail" />
                    <asp:BoundField DataField="adres" HeaderText="Adres" ItemStyle-CssClass="jasnycenter" HeaderStyle-CssClass="jasnycenter" />
                    <asp:BoundField DataField="data_urodzenia" HeaderText="Data ur." DataFormatString="{0:d}" />
                    <asp:BoundField DataField="pesel" HeaderText="Pesel" ItemStyle-CssClass="jasnycenter" HeaderStyle-CssClass="jasnycenter" />
                    <asp:BoundField DataField="haslo" HeaderText="Hasło" Visible="False" />
                </Columns>
            </asp:GridView>
        </div>
</asp:Content>

