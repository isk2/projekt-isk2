﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class PROTECTED_AdminLayer_DodajPrzedmiotDoKierunku : System.Web.UI.Page
{
    private int idkier;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (PreviousPage != null)
        {

        }
        //    Button1.Enabled = false;
        if (!Page.IsPostBack)
        {
            PobierzPrzedmioty();
            PobierzWykladowcow();
        }

    }
    protected void DodajPrzedmiotDoKierunku(int idkierunku, int idprzedmiotu, int idwykladowcy, int semestr, string rokakademicki)
    {
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand("INSERT INTO PrzedmiotWykladowca (idprzedmiotu, idwykladowcy, semestr, rokakademicki, idkierunku) values(@idprzedmiotu, @idwykladowcy, @semestr, @rokakademicki, @idkierunku)",connection);
        command.Parameters.AddWithValue("@idprzedmiotu", idprzedmiotu);
        command.Parameters.AddWithValue("@idwykladowcy",idwykladowcy);
        command.Parameters.AddWithValue("@semester", semestr);
        command.Parameters.AddWithValue("@rokakademicki", rokakademicki);
        command.Parameters.AddWithValue("@idkierunku", idkierunku);
        connection.Open();
        command.CommandType = CommandType.Text;
        command.ExecuteNonQuery();
        connection.Close();
        Response.Write("<script>alert('Połączono przedmiot z kierunkiem');</script>");

    }

    protected void PobierzPrzedmioty()
    {
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("select Id, nazwa from Przedmioty");
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        if (dt.Rows.Count == 0)
        {
            DropDownList1.DataSource = dt;
            DropDownList1.DataValueField = "Id";
            DropDownList1.DataTextField = "nazwa";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, new ListItem("Brak przedmiotów", String.Empty));
        }
        else
        {

            DropDownList1.DataSource = dt;
            DropDownList1.DataValueField = "Id";
            DropDownList1.DataTextField = "nazwa";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, new ListItem("Wybierz Przedmiot", String.Empty));
        }
    }
    protected void PobierzWykladowcow()
    {
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("select Id, imie +' '+ nazwisko AS nazwa from Wykladowcy");
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();

        adapter.Fill(dt);
        if (dt.Rows.Count == 0)
        {
            DropDownList2.DataSource = dt;
            DropDownList2.DataValueField = "Id";
            DropDownList2.DataTextField = "nazwa";
            DropDownList2.DataBind();
            DropDownList2.Items.Insert(0, new ListItem("Brak wykładowców", String.Empty));
        }
        else
        {
            DropDownList2.DataSource = dt;
            DropDownList2.DataValueField = "Id";
            DropDownList2.DataTextField = "nazwa";
            DropDownList2.DataBind();
            DropDownList2.Items.Insert(0, new ListItem("Wybierz Wykładowce", String.Empty));
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int przedmiot, wykladowca;
        przedmiot = DropDownList1.SelectedIndex;
        wykladowca = DropDownList2.SelectedIndex;
        idkier = Convert.ToInt32((string)(Session["idkierunku"]));
        if (przedmiot != 0 && wykladowca != 0 && !String.IsNullOrEmpty(TextBox1.Text) && !String.IsNullOrEmpty(TextBox2.Text))
        {
            DodajPrzedmiotDoKierunku(idkier, DropDownList1.SelectedIndex, DropDownList2.SelectedIndex, Convert.ToInt32(TextBox1.Text), TextBox2.Text);
            Server.Transfer("Kierunki.aspx");
        }
        else
        {
            Response.Write("<script>alert('Błędnie wypełnione dane');</script>");
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Server.Transfer("Kierunki.aspx");
    }
}