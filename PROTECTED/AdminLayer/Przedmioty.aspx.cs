﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PROTECTED_AdminLayer_Przedmioty : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PolaczPrzedmiot();
        }
    }

    protected void PolaczPrzedmiot()
    {
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("select * from Przedmioty");
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        GridView_Przedmioty.DataSource = dt;
        GridView_Przedmioty.DataBind();
        GridView_Przedmioty.Visible = true;
        if (dt.Rows.Count != 0)
        {
            Label_brak_rekordu.Text = "";
        }
        else
        {
            Label_brak_rekordu.Text = "Brak rekordów";
        }
        connection.Close();
    }

    protected void SzukajPrzedmiot()
    {
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        String query = "select * from Przedmioty where nazwa like @Param";
        SqlCommand command = new SqlCommand(query,connection);
        command.Parameters.AddWithValue("@Param", "%"+TextBox_Szukaj_Przedmiot.Text+"%");
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        GridView_Przedmioty.DataSource = dt;
        GridView_Przedmioty.DataBind();
        GridView_Przedmioty.Visible = true;
        if (dt.Rows.Count == 0)
        {
            Label_brak_rekordu.Text = "Brak rekordu";
        }
        else
        {
            Label_brak_rekordu.Text = "";
        }
        connection.Close();
        
    }
    /*
     kod odpowiedzialny za edycję przedmiotu
     */
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView_Przedmioty.EditIndex = e.NewEditIndex;
        PolaczPrzedmiot();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView_Przedmioty.EditIndex = -1;
        PolaczPrzedmiot();
    }

    /*
     kod odpowiedzialny za kasowanie przedmiotu
     */
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        String id = GridView_Przedmioty.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand("delete from Przedmioty where Id=@Id",connection);
        command.Parameters.AddWithValue("@Id", id);
        command.CommandType = CommandType.Text;
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        PolaczPrzedmiot();
    }
    /*
     aktualizacja danych po edycji przedmiotu
     */
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        String id = GridView_Przedmioty.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connectionString);

        String query ="update Przedmioty set nazwa=@nazwa, wyk_l_godzin=@wyk_l_godzin, lab_l_godzin=@lab_l_godzin, egzamin=@egzamin, rodzaj=@rodzaj where id=@id";
        SqlCommand command = new SqlCommand(query,connection);
        for (int i = 0; i < GridView_Przedmioty.Columns.Count; i++)
        {
            DataControlFieldCell objCell = (DataControlFieldCell)GridView_Przedmioty.Rows[e.RowIndex].Cells[i];
            GridView_Przedmioty.Columns[i].ExtractValuesFromCell(e.NewValues, objCell, DataControlRowState.Edit, false);
        }
        bool blad=false;
        try
        {
            int wyk_l_godz = Convert.ToInt32(e.NewValues["wyk_l_godzin"]);
            int lab_l_godz = Convert.ToInt32(e.NewValues["lab_l_godzin"]);
        }
        catch(Exception ex)
        {
            blad = true;
        }
        if (e.NewValues["rodzaj"].ToString().Length == 0 || e.NewValues["nazwa"].ToString().Length == 0 || e.NewValues["wyk_l_godzin"].ToString().Length == 0 || e.NewValues["lab_l_godzin"].ToString().Length == 0 )
        {
            blad = true;
        }
        if (!blad)
        {
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@nazwa", e.NewValues["nazwa"]);
            command.Parameters.AddWithValue("@wyk_l_godzin", e.NewValues["wyk_l_godzin"]);
            command.Parameters.AddWithValue("@lab_l_godzin", e.NewValues["lab_l_godzin"]);
            command.Parameters.AddWithValue("@egzamin", e.NewValues["egzamin"]);
            command.Parameters.AddWithValue("@rodzaj", e.NewValues["rodzaj"]);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
            GridView_Przedmioty.EditIndex = -1;
            PolaczPrzedmiot(); 
            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + "Pomyślnie zaktualizowano" + "');", true);
        }
        else
        {

            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + "Wystąpił błąd" + "');", true);
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Przedmioty.PageIndex = e.NewPageIndex;
        PolaczPrzedmiot();
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Server.Transfer("DodajPrzedmiot.aspx");
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        SzukajPrzedmiot();
    }
}