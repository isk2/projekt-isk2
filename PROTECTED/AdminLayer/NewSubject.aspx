﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/AdminLayer/MasterAdmin.master" AutoEventWireup="true" CodeFile="NewSubject.aspx.cs" Inherits="PROTECTED_AdminLayer_NewSubject" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div><h1>Dodaj przedmiot do kierunku</h1></div>
    <div id="dodajprzedmiotn">
        <span id="pierwszy"><p>Nazwa przedmiotu: </p><asp:DropDownList ID="DropDownList1" runat="server" ></asp:DropDownList></span>
        <span><p>ID Kierunku:</p><asp:TextBox ID="IdKierunku" runat="server"></asp:TextBox></span>
        <asp:RegularExpressionValidator runat="server" id="IdKierunkuReg" controltovalidate="IdKierunku" Display="Dynamic" validationexpression="^[1-30]" errormessage="Id kierunku to jednocyfrowa wartość  ; cyfra arabska" />
        <span><p>ID Wykladowcy:</p><asp:TextBox ID="IdWykladowcy" runat="server" ></asp:TextBox></span>
        <asp:RegularExpressionValidator runat="server" id="RegularExpressionValidator1" controltovalidate="IdWykladowcy" Display="Dynamic" validationexpression="^[1-30]" errormessage="Id kierunku to jednocyfrowa wartość  ; cyfra arabska" />
        <span><p>Semestr:</p><asp:TextBox ID="Semestr" runat="server" ></asp:TextBox></span>
        <asp:RegularExpressionValidator runat="server" id="RegularExpressionValidator2" controltovalidate="Semestr" Display="Dynamic" validationexpression="^[1-7]{1}" errormessage="Semestr to jedna liczba dziesiętna" />
        <span><p>Rok Akademicki:</p><asp:TextBox ID="RokAkademicki" runat="server" ></asp:TextBox></span>
        <asp:RegularExpressionValidator runat="server" ID="rokAkademickiWalidator" ControlToValidate="RokAkademicki" Display="Dynamic" ValidationExpression="^[2]{1}[0]{1}[0-9]{2}" ErrorMessage="Rok akademicki to rok, w którym rozpoczyna się rok akademicki" />
        <span id="przyciski"><asp:Button ID="Button1" runat="server" Text="Dodaj Przedmiot" OnClick="Button1_Click" /></span>
    </div>
</asp:Content>
