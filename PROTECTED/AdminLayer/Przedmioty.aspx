﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/AdminLayer/MasterAdmin.master" AutoEventWireup="true" CodeFile="Przedmioty.aspx.cs" Inherits="PROTECTED_AdminLayer_Przedmioty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div><h1>Przedmioty</h1></div>
    <div>
        <asp:TextBox ID="TextBox_Szukaj_Przedmiot" runat="server" Width="378px" Text="Nazwa przedmiotu" TextMode="Search" ToolTip="Wpisz nazwę przedmiotu..."></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Szukaj" OnClick="Button1_Click" />
         <asp:Button ID="Button2" runat="server" Text="Dodaj" OnClick="Button2_Click" /> 
    </div>
    <div>
        <asp:GridView ID="GridView_Przedmioty" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" ForeColor="white" HeaderStyle-BackColor="#180202" FooterStyle-BackColor="#180202" GridLines="Horizontal" RowStyle-Wrap="true" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">
            <AlternatingRowStyle BackColor="#7F4849" ForeColor="#ffeded" />
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" ItemStyle-CssClass="id" />
                <asp:BoundField DataField="nazwa" HeaderText="Nazwa przedmiotu" ItemStyle-CssClass="nazwaprzedmiotu" HeaderStyle-CssClass="nazwaprzedmiotu" />
                <asp:BoundField DataField="wyk_l_godzin" HeaderText="Liczba godzin wykładów" ItemStyle-CssClass="lgodzwykl" HeaderStyle-CssClass="lgodzwykl" />
                <asp:BoundField DataField="lab_l_godzin" HeaderText="Liczba godzin laboratorium" ItemStyle-CssClass="lgodzlab" HeaderStyle-CssClass="lgodzlab" />
                <asp:CheckBoxField DataField="egzamin" HeaderText="Egzamin" ItemStyle-CssClass="egzamin" HeaderStyle-CssClass="egzamin" />
                <asp:BoundField DataField="rodzaj" HeaderText="Rodzaj przedmiotu" ItemStyle-CssClass="rodzajprzedmiotu" HeaderStyle-CssClass="rodzajprzedmiotu" />
                <asp:CommandField ShowEditButton="true" ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="http://www.cerchem.pl/test/usun.png" EditImageUrl="http://www.cerchem.pl/test/zmien.png" CancelImageUrl="http://www.cerchem.pl/test/anuluj.png" UpdateImageUrl="http://www.cerchem.pl/test/ok.png" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="przyciski"  />
            </Columns>
        </asp:GridView>
        <asp:Label ID="Label_brak_rekordu" runat="server"></asp:Label>
    </div>
</asp:Content>

