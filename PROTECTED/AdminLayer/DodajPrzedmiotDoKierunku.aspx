﻿<%@ Page Language="C#" MasterPageFile="~/PROTECTED/AdminLayer/MasterAdmin.master" AutoEventWireup="true" CodeFile="DodajPrzedmiotDoKierunku.aspx.cs" Inherits="PROTECTED_AdminLayer_DodajPrzedmiotDoKierunku" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div><h1>Połącz przedmiot z kierunkiem</h1></div>
    <div id="dodajprzedmiotdokierunku">
        <span id="pierwszy"><p>Nazwa Przedmiotu:</p><asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList></span>
        <span><p>Wykładowca:</p><asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList></span>
        <span><p>Semestr:</p><asp:TextBox ID="TextBox1" runat="server"  ></asp:TextBox></span>
        <asp:RegularExpressionValidator runat="server" id="semestrReg" controltovalidate="TextBox1" Display="Dynamic" validationexpression="^[1-7]{1}" errormessage="Semestr należy wyrazić w postaci jpojedynczej cyfry arabskiej" />
        <span><p>Rok akademicki:</p><asp:TextBox ID="TextBox2" runat="server"  ></asp:TextBox></span>
        <asp:RegularExpressionValidator runat="server" id="rokAkademickiReg" controltovalidate="TextBox2" Display="Dynamic" validationexpression="^[2]{1}[0]{1}[0-9]{2}" errormessage="rok adademicki wyrazić w postaci roku rozpoczęcia studiów" />
        <span id="przyciski"><asp:Button ID="Button1" runat="server" Text="Dodaj Przedmiot" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Cofnij" /></span>
    </div>
</asp:Content>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div><h1>Połącz przedmiot z kierunkiem</h1></div>
    <div id="dodajprzedmiotdokierunku">
        <span id="pierwszy"><p>Nazwa Przedmiotu:</p><asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList></span>
        <span><p>Wykładowca:</p><asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList></span>
        <span><p>Semestr:</p><asp:TextBox ID="TextBox1" runat="server"  ></asp:TextBox></span>
        <span><p>Rok akademicki:</p><asp:TextBox ID="TextBox2" runat="server"  ></asp:TextBox></span>
        <span id="przyciski"><asp:Button ID="Button1" runat="server" Text="Dodaj Przedmiot" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Cofnij" /></span>
    </div>
</asp:Content>--%>