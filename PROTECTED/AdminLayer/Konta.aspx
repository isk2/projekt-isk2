﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/AdminLayer/MasterAdmin.master" AutoEventWireup="true" CodeFile="Konta.aspx.cs" Inherits="PROTECTED_AdminLayer_Konta" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Konta</h1>
    <div>
        <h3 style="margin-left: 43%; color:#d28383;">Konta studentów</h3>
            <asp:TextBox ID="TextBox1" runat="server" Text="Wyszukaj..." ></asp:TextBox>
            <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="white" HeaderStyle-BackColor="#180202" FooterStyle-BackColor="#180202" GridLines="Horizontal" RowStyle-Wrap="true" AutoGenerateColumns="False" DataKeyNames="Id" OnRowDeleting="GridView1_RowDeleting" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" EnableModelValidation="True" ShowFooter="True">
                <AlternatingRowStyle BackColor="#7F4849" ForeColor="#ffeded" />
                <Columns>
                    <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="http://www.cerchem.pl/test/zmien.png"  CancelImageUrl="http://www.cerchem.pl/test/anuluj.png" UpdateImageUrl="http://www.cerchem.pl/test/ok.png" ItemStyle-CssClass="normalcenter" HeaderStyle-CssClass="normalcenter" />
                    <asp:BoundField DataField="Id" HeaderText="id" Visible="False" />
                    <asp:BoundField DataField="nr_indeksu" HeaderText="Nr indeksu" ItemStyle-CssClass="footernrindeksu" HeaderStyle-CssClass="footernrindeksu" FooterStyle-CssClass="footernrindeksu" />
                    <asp:BoundField DataField="imie" HeaderText="Imię" ItemStyle-CssClass="footerimie" HeaderStyle-CssClass="footerimie" FooterStyle-CssClass="footerimie" />
                    <asp:BoundField DataField="nazwisko" HeaderText="Nazwisko" ItemStyle-CssClass="footernazwisko" HeaderStyle-CssClass="footernazwisko"  FooterStyle-CssClass="footernazwisko" />
                    <asp:BoundField DataField="idkierunku" HeaderText="Kierunek" ItemStyle-CssClass="footerkierunek" HeaderStyle-CssClass="footerkierunek" FooterStyle-CssClass="footerkierunek" />
                    <asp:BoundField DataField="aktualny_sem" HeaderText="Semestr" ItemStyle-CssClass="footersemestr" HeaderStyle-CssClass="footersemestr"  FooterStyle-CssClass="footersemestr" />
                    <asp:BoundField DataField="tryb_studiow" HeaderText="Tryb" ItemStyle-CssClass="footertryb" HeaderStyle-CssClass="footertryb" FooterStyle-CssClass="footertryb" />
                    <asp:BoundField DataField="data_rozpoczecia" HeaderText="Rozpoczęcie" DataFormatString="{0:d}" ItemStyle-CssClass="footerrozpoczecie" HeaderStyle-CssClass="footerrozpoczecie"  FooterStyle-CssClass="footerrozpoczecie" />
                    <asp:BoundField DataField="mail" HeaderText="E-mail" ItemStyle-CssClass="footermail" HeaderStyle-CssClass="footermail" FooterStyle-CssClass="footermail" />
                    <asp:BoundField DataField="adres" HeaderText="Adres" ItemStyle-CssClass="footeradres" HeaderStyle-CssClass="footeradres"  FooterStyle-CssClass="footeradres" />
                    <asp:BoundField DataField="data_urodzenia" HeaderText="Data ur." DataFormatString="{0:d}" ItemStyle-CssClass="footerur" HeaderStyle-CssClass="footerur" FooterStyle-CssClass="footerur" />
                    <asp:BoundField DataField="pesel" HeaderText="Pesel" ItemStyle-CssClass="footerpesel" HeaderStyle-CssClass="footerpesel"  FooterStyle-CssClass="footerpesel" />
                    <asp:BoundField DataField="haslo" HeaderText="Hasło" ItemStyle-CssClass="haslo" HeaderStyle-CssClass="normalcenter"/>
                    <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="http://www.cerchem.pl/test/usun.png" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ItemStyle-CssClass="przyciski" HeaderStyle-CssClass="normalcenter"/>
                </Columns>
            </asp:GridView>
    </div>
    <br />
    <br />
     <div>
        <h3 style="margin-left: 42%; color:#d28383;">Konta wykładowców</h3>
             <br />
         <br />
            <asp:GridView ID="GridView2" runat="server" CellPadding="4" ForeColor="white" GridLines="Horizontal" FooterStyle-BackColor="#180202" HeaderStyle-BackColor="#180202" BorderStyle ="Outset" BorderColor="White" AutoGenerateColumns="False"  DataKeyNames="Id" OnRowDeleting="GridView2_RowDeleting" OnRowCancelingEdit="GridView2_RowCancelingEdit" OnRowEditing="GridView2_RowEditing" OnRowUpdating="GridView2_RowUpdating" EnableModelValidation="True" ShowFooter="True">
                <AlternatingRowStyle BackColor="#7F4849" ForeColor="#ffeded" />
                <Columns>
                    <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="http://www.cerchem.pl/test/zmien.png"  CancelImageUrl="http://www.cerchem.pl/test/anuluj.png" UpdateImageUrl="http://www.cerchem.pl/test/ok.png" ItemStyle-CssClass="normalcenter" HeaderStyle-CssClass="normalcenter" />
                    <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" ItemStyle-CssClass="wyklid" HeaderStyle-CssClass="wyklid" FooterStyle-CssClass="wyklidfoot" />
                    <asp:BoundField DataField="imie" HeaderText="Imię" ItemStyle-CssClass="wyklimie" HeaderStyle-CssClass="wyklimie" FooterStyle-CssClass="wyklimiefoot" />
                    <asp:BoundField DataField="nazwisko" HeaderText="Nazwisko" ItemStyle-CssClass="wyklnazw" HeaderStyle-CssClass="wyklnazw" FooterStyle-CssClass="wyklnazwfoot" />
                    <asp:BoundField DataField="katedra" HeaderText="Katedra" ItemStyle-CssClass="wyklkatedra" HeaderStyle-CssClass="wyklkatedra" FooterStyle-CssClass="wyklkatedrafoot" />
                    <asp:BoundField DataField="email" HeaderText="E-mail" ItemStyle-CssClass="wyklmail" HeaderStyle-CssClass="wyklmail" FooterStyle-CssClass="wyklmailfoot" />
                    <asp:BoundField DataField="login" HeaderText="Login" ItemStyle-CssClass="wykllogin" HeaderStyle-CssClass="wykllogin" FooterStyle-CssClass="wyklloginfoot" />
                    <asp:BoundField DataField="haslo" HeaderText="Hasło" ItemStyle-CssClass="wyklhaslo" HeaderStyle-CssClass="wyklhaslo" FooterStyle-CssClass="wyklhaslofoot" />
                    <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="http://www.cerchem.pl/test/usun.png" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ItemStyle-CssClass="przyciski" FooterStyle-CssClass="wyklusunfoot" />
                </Columns>
            </asp:GridView>
    </div>
</asp:Content>

