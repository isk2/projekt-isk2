﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PROTECTED_AdminLayer_MasterAdmin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string menuName = "";
        string menuContent = "";
        if (!Logiczna.Administrator)
        {
            Response.Redirect("~/Logowanie.aspx");
        }
        if (Logiczna.Administrator == true)
        {

            menuName = "Administrator";
            menuContent = "<li class=\"\">" +
                             "<a href=\"../AdminLayer/DodajPrzedmiot.aspx\"><span>Dodaj Przedmiot</span></a>" +
                             "<a href=\"../AdminLayer/NewSubject.aspx\"><span>Dodaj Przedmiot Do Kierunku</span></a>" +
                             "<a href=\"../AdminLayer/DodajPrzedmiotDoKierunku.aspx\"><span>Dołącz Przedmiot Do Kierunku</span></a>" +
                             "<a href=\"../AdminLayer/Kierunki.aspx\"><span>Kierunki</span></a>" +
                             "<a href=\"../AdminLayer/Konta.aspx\"><span>Konta</span></a>" +
                             "<a href=\"../AdminLayer/Przedmioty.aspx\"><span>Przedmioty</span></a>" +
                          "</li>";
        }
        string content = "<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">{0}<span class=\"caret\"></span></a><ul class=\"dropdown-menu\">{1}</ul>";

        menu_li.InnerHtml = string.Format(content, menuName, menuContent);
		
    }
    protected void Wyloguj_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Session.Abandon();
        Session.RemoveAll();
        Response.Redirect("~/Logowanie.aspx");
    }
}
