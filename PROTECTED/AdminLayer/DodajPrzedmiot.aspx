﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/AdminLayer/MasterAdmin.master" AutoEventWireup="true" CodeFile="DodajPrzedmiot.aspx.cs" Inherits="PROTECTED_AdminLayer_DodajPrzedmiot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div><h1>Dodaj Przedmiot</h1></div>
    <div id="dodajprzedmiot">
        <span id="pierwszy"><p>Nazwa Przedmiotu:</p><asp:TextBox ID="TextBox_nazwa" runat="server"></asp:TextBox></span>
        <asp:RegularExpressionValidator runat="server" id="regPrzedmiot" controltovalidate="TextBox_nazwa" Display="Dynamic" validationexpression="[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ ]{5,50}" errormessage="Nazwa kierunku składa się tylko z liter(minimum 5)." />
        <span><p>Liczba godzin wykładu:</p><asp:TextBox ID="TextBox_wyk_l_godzin" runat="server"></asp:TextBox></span>
        <asp:RegularExpressionValidator runat="server" id="godzinyWykladuReg" controltovalidate="TextBox_wyk_l_godzin" Display="Dynamic" validationexpression="^([0-5]?[0-9]|60)$" errormessage="Maksymalna wartość liczby godzin wykładów to 60, wyrażona za pomocą liczby" />
        <span><p>Liczba godzin laboratorium:</p><asp:TextBox ID="TextBox_lab_l_godzin" runat="server"></asp:TextBox></span>
        <asp:RegularExpressionValidator runat="server" id="GodzinyLabReg" controltovalidate="TextBox_lab_l_godzin" Display="Dynamic" validationexpression="^([0-1]?[0-5]|[0-9])$" errormessage="Maksymalna wartość liczby godzin laboratoriów to 15, wyrażona za pomocą liczby" />
        <span><p>Egzamin:</p><asp:CheckBox ID="CheckBox_egzamin" runat="server" /></span>
        <span><p>Rodzaj Przedmiotu:</p><asp:TextBox ID="TextBox_rodzaj" runat="server"></asp:TextBox></span>
        <span id="przyciski"><asp:Button ID="Button1" runat="server" Text="Dodaj Przedmiot" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" Text="Pokaż Przedmioty" OnClick="Button2_Click" /></span>
    </div>
</asp:Content>
