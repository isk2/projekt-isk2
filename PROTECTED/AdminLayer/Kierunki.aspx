﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/AdminLayer/MasterAdmin.master" AutoEventWireup="true" CodeFile="Kierunki.aspx.cs" Inherits="PROTECTED_AdminLayer_Kierunki" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div><h1>Kierunki</h1></div>

        <h4>Szukaj kierunku</h4>
        <div id="szukanko">
            <span><asp:TextBox ID="TextBox1" runat="server" Text="Wpisz nazwę kierunku"></asp:TextBox></span>
            <asp:Button ID="Button6" runat="server" OnClick="Button6_Click" Text="Dodaj przedmiot do tego kierunku" Visible="false" />
            <asp:Button ID="Button1" runat="server" Text="Szukaj Kierunku" OnClick="Button1_Click" />
            <asp:Button ID="Button2" runat="server" Text="Dodaj Kierunek" OnClick="Button2_Click" />
            <asp:RegularExpressionValidator runat="server" id="IdKierunekReg" controltovalidate="TextBox1" Display="Dynamic" ForeColor="#cc0000" validationexpression="[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ ]{5,50}" errormessage="Nazwa kierunku składa się tylko z liter(minimum 5)." />
        </div>

    <asp:GridView ID="GridView1" runat="server" CellPadding="4" DataKeyNames="Id" EnableModelValidation="True" ForeColor="white" HeaderStyle-BackColor="#180202" FooterStyle-BackColor="#180202" GridLines="Horizontal" RowStyle-Wrap="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="False">
        <AlternatingRowStyle BackColor="#7F4849" ForeColor="#ffeded" />
        <Columns>
           <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" ItemStyle-CssClass="idkierunki" HeaderStyle-CssClass="idkierunki"  />
           <asp:BoundField DataField="nazwa" HeaderText="Nazwa kierunku" ItemStyle-CssClass="jasnycenter" HeaderStyle-CssClass="jasnycenter" />
           <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="http://www.cerchem.pl/test/usun.png" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ControlStyle-CssClass="usun" HeaderStyle-CssClass="usun" />
        </Columns>
        <SelectedRowStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
    </asp:GridView>
    
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
  
    <div id="szukanko2"><asp:Button ID="Button5" runat="server" Text="Pokaż wszystkie" OnClick="Button5_Click" /></div>

    <div id="szukanko3">Id kierunku: <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
    <asp:Button ID="Button3" runat="server" Text="Szukaj Studentów" OnClick="Button3_Click" />
    <asp:Button ID="Button4" runat="server" Text="Szukaj Przedmiotów" OnClick="Button4_Click" />
    <asp:Button ID="btnNewEntry" runat="Server" CssClass="button" Text="Dodaj przedmiot do kierunku" OnClick="btnNewEntry_Click" />
    <asp:RegularExpressionValidator runat="server" id="RegularExpressionValidator2" controltovalidate="TextBox2" Display="Dynamic" ForeColor="#cc0000" validationexpression="[0-9]{1,10}" errormessage="Id kierunku jest liczbą." />
    </div>

    <asp:GridView ID="GridView2" runat="server" EnableModelValidation="True" CellPadding="4" ForeColor="white" HeaderStyle-BackColor="#180202" FooterStyle-BackColor="#180202" GridLines="Horizontal" RowStyle-Wrap="true">
        <AlternatingRowStyle BackColor="#7F4849" ForeColor="#ffeded" />
    </asp:GridView>

</asp:Content>