﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Configuration;

public partial class PROTECTED_AdminLayer_NewSubject : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand com = new SqlCommand("select *from Przedmioty", connection); // table name 
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);  // fill dataset
            DropDownList1.DataTextField = ds.Tables[0].Columns["Nazwa"].ToString();
            DropDownList1.DataValueField = ds.Tables[0].Columns["Id"].ToString();
            DropDownList1.DataSource = ds.Tables[0];
            DropDownList1.DataBind();
            connection.Close();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string przedmiot = DropDownList1.SelectedValue;
        string kierunk = IdKierunku.Text.ToString();
        string nazwa = DropDownList1.SelectedItem.ToString();
        string idWykladowcy = IdWykladowcy.Text.ToString();
        string semestr = Semestr.Text.ToString();
        string rokAkademicki = RokAkademicki.Text.ToString();
        DodajPrzedmiot(przedmiot, idWykladowcy, semestr, rokAkademicki, kierunk, nazwa);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
    }
    

    protected void DodajPrzedmiot(string przedmiot, string idWykladowcy, string semestr, string rokAkademicki, string kierunek, string nazwa)
    {
        try
        {
            if (kierunek.Length == 0 || rokAkademicki.Length == 0 || semestr.Length == 0 || idWykladowcy.Length == 0)
            {
                throw new Exception();
            }
            String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DziekanatDBConnectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.Text;
            command.CommandText = String.Format("insert into PrzedmiotWykladowca (idprzedmiotu,idwykladowcy,semestr,rokakademicki,idkierunku) values(@idprzedmiotu,@idwykladowcy,@semestr,@rokakademicki,@idkierunku)");
            command.Parameters.AddWithValue("@idwykladowcy", Convert.ToInt32(przedmiot));
            command.Parameters.AddWithValue("@idprzedmiotu", Convert.ToInt32(idWykladowcy));
            command.Parameters.AddWithValue("@semestr", Convert.ToInt32(semestr));
            command.Parameters.AddWithValue("@rokakademicki", Convert.ToInt32(rokAkademicki));
            command.Parameters.AddWithValue("@idkierunku", Convert.ToInt32(przedmiot));
            command.Parameters.AddWithValue("@idprzedmiotu", Convert.ToInt32(kierunek));

            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
            Response.Write("<script>alert('Dodano przedmiot " + nazwa + "');</script>");
        }
        catch (Exception e)
        {

            Response.Write("<script>alert('Bład dodawania rekordu');</script>");
        }

        
        
        
    }
    



}